# Telegram Bot For Smart Interface  MQTT Support
The main idea of the project is to use the telegram as a GUI for project and needs that I run and use in my home, That way you can control in a secure way things at your house.


## Getting Started
To run the code first you need to have a telegram bot(You need a TOKEN), the bot can be created from “Bot Father” just add the bot to your telegram user list by clicking on that link https://telegram.me/botfather, and look in that tutorial https://core.telegram.org/bots#6-botfather. The process is very easy.
Second you need to install the fallowing library using pip
```
pip install python-telegram-bot
pip install lxml
pip install paho-mqtt
```
Third (optional, If you don’t use the MQTT) you need to to run a MQTT Server (Broker). I am using mosquitto as my broker, It run on the RPi. You can find on the web a good tutorial how to install.

## Deployment
Addon that I working on to improve the bot:
1. Accessing to the router vi SSH, router that run Tomato firmware. The idea is to inform you on a disconnection/reconnection and unknown connection.
2. Use OpenCV to take a picture and send it to my telegram account
3. Israel Rail Bot, that way you can ask the bot and get that in the fastest way with the minimal use of the data connection of you phone.
